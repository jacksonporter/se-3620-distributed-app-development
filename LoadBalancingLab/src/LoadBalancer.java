import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 */

/**
 * @author jacksonporter
 *
 */
public class LoadBalancer implements Runnable{
	public static Logger log = LogManager.getLogger(LoadBalancer.class);
	private static Thread[] workers;
	private static Server[] servers;
	public static volatile int currentServer = -1;
	Queue<Request> requests;
	boolean continueRunning;
	Client client;
	
	public LoadBalancer(int numofservers, int numofworkers, Client client) {
		this.client = client;
		requests = new LinkedList<Request>();
		continueRunning = true;
		
		servers = new Server[numofservers];
		for(int i = 0; i < servers.length; i++) {
			servers[i] = new Server(this);
		}
		
		workers = new Thread[numofworkers];
	}
	
	public void makeRequest(Request request) {
		this.requests.add(request);
	}

	public void makeRequest(int num) {
		this.requests.add(new Request(num));
	}
	
	public static Server getNextServer() {
		LoadBalancer.currentServer++;
		
		log.debug("LB CS NUM: " + LoadBalancer.currentServer);
		if(currentServer > (LoadBalancer.servers.length - 1)) {
			LoadBalancer.currentServer = 0;
		}
		
		return LoadBalancer.servers[LoadBalancer.currentServer];
	}

	@Override
	public void run() {
		log.info("Listener thread has been started.");
		
		while(this.continueRunning) {
			if(!requests.isEmpty()) {
				Request current = requests.remove();
				log.trace("AMOUNT NOW IN QUEUE: " + requests.size());
				
				for(int i = 0; i < this.workers.length; i++) {
					try {
						if(!workers[i].isAlive()) {
							workers[i] = new Thread(new WorkerThread(current, this));
							workers[i].start();
							current = null;
							break;
						}
					}catch(java.lang.NullPointerException e) {
						log.trace("No thread started here  (" + i + ") yet!");
						workers[i] = new Thread(new WorkerThread(current, this));
						workers[i].start();
						current = null;
						break;
					}
				}
				
				if(current != null) {
					log.debug("All threads are busy.");
					LinkedList temp = (LinkedList) requests;
					temp.addFirst(current);
				}
			}
			else {
				log.debug("No requests. Sleeping for 3 seconds.");
				
				//Sleep for 1 second ask no requests were found (free up CPU)
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					log.error("Thread was interuppted.");
					e.printStackTrace();
					
				}
			}
		}
	}

	public void printResponse(String message) {
		client.addMessage(message);
	}
}
