import java.util.LinkedList;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 */

/**
 * @author jacks
 *
 */
public class Client implements Runnable{
	public static Logger log = LogManager.getLogger(Client.class);
	private LoadBalancer lb;
	private Thread lbl;
	private static volatile Queue<String> messages;
	
	public Client() {
		lb = new LoadBalancer(4, 4, this);
		messages = new LinkedList<String>();
		lbl = new Thread(lb);
		
		for(int i = 0; i < 1000; i++)
		{
			lb.makeRequest(i * 1000);
		}
	}
	
	public void addMessage(String message) {
		this.messages.add(message);
	}

	@Override
	public void run() {
		lbl.start();
		log.info("Started load balancer and listener thread.");

		
		while(true) {
			if(messages.size() > 0) {
				log.info("" + messages.remove());
			}
		}
	}
	
	
}
