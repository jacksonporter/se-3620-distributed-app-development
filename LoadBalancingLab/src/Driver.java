/**
 * 
 */
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * @author jacksonporter
 *
 */
public class Driver {
	public static Logger log = LogManager.getLogger(Driver.class);
	
	
	/**
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		log.info("Program started.");
		Client client = new Client();
		Thread clientThread = new Thread(client);
		clientThread.start();
	}
}
