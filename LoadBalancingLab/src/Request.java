/**
 * 
 */

/**
 * @author jacksonporter
 *
 */
public class Request {
	String message;
	int num;
	
	public Request(String message) {
		this.message = message;
		this.num = 0;
	}
	
	public Request(int num) {
		this.message = "" + num;
		this.num = num;
	}
	
	public Request(String message, int num) {
		this.message = message;
		this.num = num;
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public int getNum() {
		return this.num;
	}
}
