import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 */

/**
 * @author jacksonporter
 *
 */
public class WorkerThread implements Runnable {
	public static Logger log = LogManager.getLogger(WorkerThread.class);
	private Request request;
	private LoadBalancer lb;
	
	public WorkerThread(Request request, LoadBalancer lb) {
		this.lb = lb;
		this.request = request;
	}
		
	@Override
	public void run() {
		log.info("Worker spawned. Request: " + request.getMessage());
		
		Server currentServer = LoadBalancer.getNextServer();
		currentServer.setRequest(this.request);
		Thread serverThread = new Thread(currentServer);
		serverThread.start();
		
		try {
			serverThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		log.trace("Worker thread dead.");
	}
}
