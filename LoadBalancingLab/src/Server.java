import java.util.Random;

/**
 * 
 */

/**
 * @author jacksonporter
 *
 */
public class Server implements Runnable {
	private Request request;
	private LoadBalancer lb;
	Random rand;
	
	public Server(LoadBalancer lb) {
		this.lb = lb;
		request = null;
		
		rand = new Random();
	}
	
	public void setRequest(Request request) {
		this.request = request;
	}

	@Override
	public void run() {
		if(request.getNum() == 0) {
			int result = rand.nextInt(1001) % 1000;
			while(result != 0){
				result = rand.nextInt(1001) % 1000;
			}
			
			lb.printResponse("Request has been completed: " + request.getMessage());	
			this.request = null;
		}
		else {
			int result = rand.nextInt(request.getNum() + 1) % request.getNum();
			while(result != 0){
				result = rand.nextInt(request.getNum() + 1) % request.getNum();
			}
			
			lb.printResponse("Request has been completed: " + request.getMessage());	
			this.request = null;
			//log.info("Request has been completed: " + request.getMessage());
		}
		
	}
}
