/**
 * This is the table class that contains philosophers as well as the forks. 
 * @author jackson.porter
 *
 */
public class Table {
	private Philosopher[] philosophers;
	private static volatile Fork[] forks;
	private Thread[] threads;
	
	/**
	 * Table constructor that takes in the number of philosophers and forks to create. 
	 * @param numofphilosophers Number of philosophers
	 * @param numofforks Number of forks
	 */
	public Table(int numofphilosophers, int numofforks) {
		philosophers = new Philosopher[numofphilosophers];
		forks = new Fork[numofforks];
		threads = new Thread[numofphilosophers];
		
		for(int i = 0; i < forks.length; i++) {
			forks[i] = new Fork(i);
		}
		
		for(int i = 0; i < philosophers.length; i++) {
			if(i == (philosophers.length - 1)) {
				philosophers[i] = new Philosopher(i, forks[i], forks[0]);
			}
			else {
				philosophers[i] = new Philosopher(i, forks[i], forks[i + 1]);
			}
		}
		
		for(int i = 0; i < threads.length; i++) {
			threads[i] = new Thread(philosophers[i]);
			threads[i].start();
		}
		
		
	}

	public void letEatingBegin() {
		for(Thread thread : threads) {
			//thread.start();
			if(thread == null)
			{
				System.out.println("I am null.");
			}
		}
		
	}
}
