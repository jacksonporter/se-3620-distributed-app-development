
public class Fork {
	private boolean inUse;
	private int id;
	private int philID;
	
	public Fork(int id)
	{
		this.id = id;
		this.inUse = false;
	}
	
	private synchronized boolean inUse() {
		return this.inUse;
	}
	
	public synchronized boolean setInUse(int philID) {
		if(!this.inUse()) {
			this.philID = philID;
			this.inUse = true;
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	public int getID() {
		return this.id;
	}
	
	public synchronized void release(int philID) {
		if(this.philID == philID) {
			this.inUse = false;
		}
	}
}
