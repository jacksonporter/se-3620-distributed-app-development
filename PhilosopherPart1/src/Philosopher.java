import java.util.Random;

public class Philosopher implements Runnable {
	private int id;
	private Fork fork1, fork2;
	private boolean eating;
	
	public Philosopher(int id, Fork fork1, Fork fork2)
	{
		this.id = id;
		this.fork1 = fork1;
		this.fork2 = fork2;
		this.eating = false;
	}
	
	@Override
	public void run() {
		//System.out.println("Philosopher " + this.id + " is awake.");
		Random myRand = new Random();
		
		
		while(true) {
			//Think
			try {
				eating = false;
				System.out.println("I am thinking, and I am philosopher " + this.id + ".");
				Thread.sleep(myRand.nextInt(7000) + 3000);
			} catch (InterruptedException e) {
				System.err.println("Philosopher " + this.id + " thread was interuppted in think stage.");
				e.printStackTrace();
			}
			//Eat (when you can)
			
			while(!eating) {
				if(fork1.setInUse(this.id)) {
					System.out.println("I, Philosopher " + this.id + " have fork " + fork1.getID());
					
					while(!eating) {
						if(fork2.setInUse(this.id)) {
							eating = true;
							System.out.println("I, Philosopher " + this.id + " have fork " + fork2.getID());
							System.out.println("I, Philosopher " + this.id + ", am eating with Fork " + fork1.getID() + " and Fork " + fork2.getID());
							try {
								Thread.sleep(myRand.nextInt(7000) + 3000);
							} catch (InterruptedException e) {
								System.err.println("Philosopher " + this.id + " thread was interuppted in the eating stage.");
								e.printStackTrace();
							}
							fork1.release(this.id);
							fork2.release(this.id);
							System.out.println("I, Philosopher " + this.id + ", released Fork " + fork1.getID() + " and Fork " + fork2.getID());
						}
					}
				}
			}
		}
	}
}

