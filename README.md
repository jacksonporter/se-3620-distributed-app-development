# SE 3620 - Distributed App Development

![](https://www.snow.edu/pr/brand/images/signature.jpg)

##Distributed Application Development
######Content from the course, including course work and other misc. work. Most work in this repository is in the Java programming language.

This class taught how to make simple distributed systems, by teaching the basics of networking and simple applications communting over TCP/IP as well as UDP.

Break down of applications/projects:

- EquationClient: Android Application to send equations to a remote server, and get the result of the equation/display to user.

- JeopardyGame: A networked game, with clients and remote server. Allows for up to 3 players to play and rejects other players from entering the game.

- LoadBalancingLab: Simulation of what a load balancer can do and how to make it work (on a non-networked level).

- Philosopher Projects: Each Philosopher has a fork on the left and right, but needs both to eat. Everyone needs to take a turn! (Practices the reality of deadlocks). Different changes and parts of the project are listed.

- TestTCPProject: A sample application that allows for two running Java applications to comunciate over the network using TCP sockets.

- TestUDPProject (has a mispell): A sample application that allows for two running Java applications to comunciate over the network using UDP sockets.

