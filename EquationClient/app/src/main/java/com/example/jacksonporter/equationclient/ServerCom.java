package com.example.jacksonporter.equationclient;

import java.io.IOException;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.OutputStream;
import java.net.InetAddress;
import java.io.DataInputStream;
import java.io.InputStream;

/**
 * Created by jackson.porter on 2/27/2018.
 */

public class ServerCom {
    InetAddress serverHost;
    int serverPortNumber;
    Socket s;
    InputStream sIn;
    DataInputStream socketDIS;
    OutputStream socketOutStream;
    DataOutputStream socketDOS;
    String serverResponse;


    public ServerCom(InetAddress ip, int portNum) throws IOException {
        serverHost = ip;
        serverPortNumber = portNum;

        //Create a socket object based on given host and port number
        s = new Socket(serverHost, serverPortNumber);


        //Create objects to accept input
        sIn = s.getInputStream();
        socketDIS = new DataInputStream(sIn);


        //Create objects to send output
        socketOutStream = s.getOutputStream();
        socketDOS = new DataOutputStream(socketOutStream);
    }

    public void send(String s) throws IOException {
        socketDOS.writeUTF(s);
    }

    public String getResponse() throws IOException {
        return socketDIS.readUTF();
    }

    public void close() throws IOException {
        socketDOS.close();
        socketOutStream.close();
        socketDIS.close();
        sIn.close();
        s.close();
    }
}
