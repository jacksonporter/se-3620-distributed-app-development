//This file contains the driver for the UPD Test program between two Amazon Lightsail Instances

import java.io.IOException;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.OutputStream;
import java.net.InetAddress;
import java.io.DataInputStream;
import java.io.InputStream;

public class ClientDriver{
	public static void main(String[] args) throws IOException{
		System.out.println("Hello World! I am complied and have run!");

		if (args.length < 3) {
			System.out.println("Please pass the arguments in the order: test message, Server host name, and port number");
			//exit program if no arguments were passed.
			System.exit(1);
		}
		
		InetAddress serverHost = InetAddress.getByName(args[1]);
		int serverPortNumber = Integer.valueOf(args[2]).intValue();

		//Create a socket object based on given host and port number
		Socket s = new Socket(serverHost, serverPortNumber);


		//Create objects to accept input
		InputStream sIn = s.getInputStream();
		DataInputStream socketDIS = new DataInputStream(sIn);


		//Create objects to send output
		OutputStream socketOutStream = s.getOutputStream();
		DataOutputStream socketDOS = new DataOutputStream(socketOutStream);

		//Send equation to be evaluated.
		socketDOS.writeUTF(args[0]);

		//Obtain the message from the server
		String serverResponse = socketDIS.readUTF();
	
		//Print to console the message from the server.
		System.out.println("Server: " + serverResponse);

		//Cleanup
                socketDOS.close();
                socketOutStream.close();
				socketDIS.close();
				sIn.close();
                s.close();
        }
}
