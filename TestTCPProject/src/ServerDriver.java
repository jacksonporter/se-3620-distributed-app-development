 //This file contains the driver for the TCP Test program between two Amazon Lightsail Instances

import java.io.IOException;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class ServerDriver{
	public static void main(String[] args) throws IOException{
		System.out.println("Hello World! I am complied and have run!");
		System.out.println("Starting loop");
		
		if (args.length <= 0) {
			System.out.println("Please pass the port number for the UDPServer");
			//exit program if no arguments were passed.
			System.exit(1);
		}
		
		int socketNumber = Integer.valueOf(args[0]).intValue();
		ServerSocket ss = new ServerSocket(socketNumber);                

               
		Socket s = ss.accept();

		//Create objects to accept input
		InputStream sIn = s.getInputStream();
		DataInputStream socketDIS = new DataInputStream(sIn);


		//Create objects to send output
		OutputStream socketOutStream = s.getOutputStream();
		DataOutputStream socketDOS = new DataOutputStream(socketOutStream);


		//socketDOS.writeUTF("I recieved a connection!");
		System.out.println("I have recieved a connection.");


                ScriptEngine engine = new ScriptEngineManager().getEngineByExtension("js");
                String toSend = "";
                String expression = "";

                
                try{
                    //Continue to accept expressions until 'q' is sent to "me."
                    while(!expression.equals("q")){
			try{
				expression = new String(socketDIS.readUTF());
	                        System.out.println("Expression was: " + expression + ". Length: " + expression.length());

	                        Object result = engine.eval(expression);

        	                System.out.println("\nAnswer:" + result);
	                        toSend = result.toString();

                        	socketDOS.writeUTF(toSend);
                	        toSend = "";
        	                expression = "";
	                        expression = new String(socketDIS.readUTF());

			}catch(java.io.EOFException e){
				System.out.println("Connection was closed by client. Resetting.");
				s = ss.accept();

		                //Create objects to accept input
		                sIn = s.getInputStream();
                		socketDIS = new DataInputStream(sIn);
		
		
		                //Create objects to send output
                		socketOutStream = s.getOutputStream();
		                socketDOS = new DataOutputStream(socketOutStream);
			}
                    }
                
                }catch(ScriptException e){
                    toSend = "ERROR: COULD NOT EVALUATE";
                    e.printStackTrace();
		}

		socketDOS.writeUTF(toSend);

		//Cleanup
                socketDOS.close();
                socketOutStream.close();
		sIn.close();
		socketDIS.close();
        s.close();
        ss.close();
		}
}
