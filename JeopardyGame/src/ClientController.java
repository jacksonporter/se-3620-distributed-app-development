import java.io.IOException;

public class ClientController {
	private ClientModel model;
	private ClientView view;
	
	public ClientController() {
		view = new ClientView(this);
		model = new ClientModel(view);
		
		Thread jeopardyclient = new Thread(model, "JeopardyClient");
		jeopardyclient.start();
	}
	
	public void sendInput(String input) throws IOException {
		model.sendInput(input);
	}
}
