import java.io.IOException;

public class ServerController {
	private JeopardyGame model;
	private ServerView view;
	
	public ServerController() throws IOException {
		view = new ServerView(this);
		
/*		model1 = new JeopardyGame(1, view);
		model2 = new JeopardyGame(2, view);
		model3 = new JeopardyGame(3, view);
		catchConnection = new JeopardyGame(4, view);
		
		//Start all threads
		model1.start();
		model2.start();
		model3.start();
		catchConnection.start();
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		/*System.out.println("ServerController:JeopardyGameClass: " + JeopardyGame.client1connected + ":" + JeopardyGame.client2connected + ":" + JeopardyGame.client3connected + "\n");*/
		
		
		model = new JeopardyGame(view);
		
		Thread client1 = new Thread(model, "1");
		client1.start();
		
		Thread client2 = new Thread(model, "2");
		client2.start();
		
		Thread client3 = new Thread(model, "3");
		client3.start();
		
		Thread rogueConnection = new Thread(model, "4");
		rogueConnection.start();
	}
	
	
	/*
	 * Server's view never needs to send input back to the model!
	 */
}
