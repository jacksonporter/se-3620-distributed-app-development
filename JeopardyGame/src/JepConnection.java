import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class JepConnection {
	ServerSocket generalServerSocket;
	Socket generalSocket;
	OutputStream generalSOut;
	DataOutputStream generalSocketDOS;
	InputStream generalSIn;
	DataInputStream generalSocketDIS;
	int id;
	String name;
	
	public JepConnection(int id) throws IOException {
		generalServerSocket = JeopardyGame.generalServerSocket;
		generalSocket = generalServerSocket.accept();
		generalSOut = generalSocket.getOutputStream();
		generalSocketDOS = new DataOutputStream(generalSOut);
		generalSIn = generalSocket.getInputStream();
		generalSocketDIS = new DataInputStream(generalSIn);
		
		this.id = id;
		
		System.out.println("ADDED CONNECTION: " + id);
	}
	
	public void writeToClient(String message) throws IOException {
		generalSocketDOS.writeUTF(message);
	}
	
	public String readFromClient() throws IOException {
		return generalSocketDIS.readUTF();
	}

	public void closeConnection() throws IOException {
		this.generalSocketDIS.close();
		this.generalSocketDOS.close();
		this.generalSIn.close();;
		this.generalSOut.close();
		this.generalSocket.close();
		this.generalServerSocket.close();
	}
	
	public int getID() {
		return this.id;
	}
	
	public void setName(String n) {
		this.name = n;
	}
	
	public String getName() {
		return this.name;
	}
}
