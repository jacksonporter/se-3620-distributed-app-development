import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class ClientModel implements Runnable{
	private boolean settingUp, goOn;
	private Socket s;
	private String ip;
	private InetAddress serverHost;
	private int serverPortNumber;
	private InputStream sIn;
	private DataInputStream socketDIS;
	private OutputStream sOut;
	private DataOutputStream socketDOS;
	private ClientView view;
	
	public ClientModel(ClientView view){
		this.view = view;
		settingUp = true;
		goOn = true;
		
		view.print("*****Jeopoardy Client*****\n");
	}
		
	public void establishConnection() throws IOException{
		view.print("Attempting to establish connection.\n\n");
		serverHost = InetAddress.getByName(ip);
		s = new Socket(serverHost, 5000);
		
		sIn = s.getInputStream();
		socketDIS = new DataInputStream(sIn);

		sOut = s.getOutputStream();
		socketDOS = new DataOutputStream(sOut);
		settingUp = false;
	}

	public void processInput(String input) throws IOException {	
		//view.print("Processing input.");
		String[] splitInput = input.split(":");
		if(splitInput[0].equals("r"))
		{
			if(splitInput[1].equals("e")) {
				view.print("ServerException: " + splitInput[2]);
				goOn = false;
			}
			else if(splitInput[1].equals("p")) {
				view.print(splitInput[2]);
			}
			else {
				view.print("INCORRECT MESSAGE FROM SERVER: " + input + " CLOSING.\n\n");
				closeConnection();
				System.exit(1);
			}
		}
		else if(splitInput[0].equals("s")) {
			if(splitInput[1].equals("s")) {
				view.print(splitInput[2]);
				view.retrieve();
			}
			else {
				view.print("INCORRECT MESSAGE FROM SERVER: " + input + " CLOSING.\n\n");
				closeConnection();
				System.exit(1);
			}
		}
		else {
			view.print("INCORRECT MESSAGE FROM SERVER: " + input + " CLOSING.\n\n");
			closeConnection();
			System.exit(1);
		}
	}

	public void sendInput(String input) throws IOException {
		if(settingUp) {
			ip = input;
			try {
				establishConnection();
			} catch (IOException e) {
				view.print("\nINCORRECT FORMAT/BAD CONNECTION.\n");
			}
		}
		else {
			socketDOS.writeUTF(input);
		}
		
	}
	
	public void closeConnection() throws IOException {
		socketDOS.close();
		socketDIS.close();
		sOut.close();
		sIn.close();
		s.close();
	}


	@Override
	public void run() {
		goOn = true;
			
		try {
			view.print("Enter the ip address of the Jeopardy server: ");
			view.retrieve();
			
			while(settingUp) {
				view.print("Enter the ip address of the Jeopardy server: ");
				view.retrieve();
			}
			
			while(goOn) {
				processInput(socketDIS.readUTF() + "\n");
			}
			
			closeConnection();

		} catch (IOException e1) {
			view.print("Server closed connection. Closing Client. Thanks for playing!\n");
			System.exit(1);
		}
		
		
		
	}

}
