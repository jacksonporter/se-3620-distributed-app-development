import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ServerDriver {
	
	public static void main(String[] args) throws IOException {		
		System.out.println("JEOPARDY SERVER V1"); //Print program message
				
		InetAddress serverIP = InetAddress.getLocalHost();
		int serverPortNumber = 5000;
		
		System.out.println("Server Static IP/Port: " + "(see public ip)" + ":" + serverPortNumber);
		System.out.println("Server Internal IP/Port: " + serverIP + ":" + serverPortNumber);
		
		
		ServerController controller = new ServerController();
	}
}
