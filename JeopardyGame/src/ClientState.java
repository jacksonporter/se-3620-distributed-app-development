
public abstract class ClientState {
	ClientModel model;
	
	public ClientState(ClientModel model) {
		this.model = model;
	}
	
	public abstract void print(String message);
	
	public abstract void retrieve();
	
	public abstract String toString();
}
