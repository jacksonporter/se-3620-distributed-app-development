import java.io.IOException;
import java.util.Scanner;

public class ServerView {	
	private ClientController controller;
	private Scanner input;
		
	public ServerView(ServerController serverController) {
		this.controller = controller;
		input = new Scanner(System.in);
	}

	public void print(String message) {
		System.out.print(message);
	}
	
	public void retrieve() throws IOException {
		controller.sendInput(input.next());
	}
}
