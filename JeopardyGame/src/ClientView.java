import java.io.IOException;
import java.util.Scanner;

public class ClientView {	
	private ClientController controller;
	private Scanner input;
	
	public ClientView(ClientController controller) {
		this.controller = controller;
		input = new Scanner(System.in);
	}
	
	public void print(String message) {
		System.out.print(message);
	}
	
	public void retrieve() throws IOException {
		controller.sendInput(input.next());
	}
}
