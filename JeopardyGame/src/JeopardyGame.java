import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class JeopardyGame implements Runnable{
	//Networking Global Variables
	static final int GENERAL_PORT = 5000;
	static final ServerSocket generalServerSocket;
	static {
		ServerSocket temp = null;
		try {
			temp = new ServerSocket(JeopardyGame.GENERAL_PORT);
		}catch(IOException e) {
			System.out.println("Static variable error on ServerSocket:generalServerSocket");
		}
		generalServerSocket = temp;
	}
	
	//Display
	static ServerView view;
	
	//Game variables
	static String winner = "";
	static String QUESTION_ONE;
	static String ANSWER_ONE;
	
	static boolean[] clientStatus, clientConnected;
	static String[] clientName;
	
	//Thread private global variables
	private int id;
	
		
	public JeopardyGame(ServerView view) throws IOException {
		this.view = view;
		
		QUESTION_ONE = "Question 1; What is the capital of Burundi?";
		ANSWER_ONE = "Bujumbura";
		
		clientStatus = new boolean[3];
		clientStatus[0] = false;
		clientStatus[1] = false;
		clientStatus[2] = false;
		
		clientConnected = new boolean[3];
		clientConnected[0] = false;
		clientConnected[1] = false;
		clientConnected[2] = false;
		
		clientName = new String[3];
		clientName[0] = "";
		clientName[1] = "";
		clientName[2] = "";
		
	}
			
	@Override
	public void run() {
		JepConnection myConnection;
		String playerName = "";
		int id = Integer.parseInt(Thread.currentThread().getName());
		int counter = 0;
		
		try {
			//view.print(Thread.currentThread().getName() + " client thread was spawned!");
			view.print(Thread.currentThread().getName() + " client thread was spawned!\n");
			
			if(id == 4) {
							
				while(!(JeopardyGame.clientConnected[0] && JeopardyGame.clientConnected[1] && JeopardyGame.clientConnected[2])) {
					//System.out.println("4: Not everyone is connected.");
					Thread.sleep(1000);
				}
				
				while(true) {
					try {
						myConnection = new JepConnection(id);
						myConnection.writeToClient("r:e:The game is currently full. Please try again later!\n");
						//myConnection.closeConnection();
					}catch (IOException e) {
						//view.print("4th connection rejected.\n");
					}
				}
			}
			else {
				myConnection = new JepConnection(id);

				JeopardyGame.clientConnected[id - 1] = true;

				myConnection.writeToClient("r:p:Hello there, player " + Thread.currentThread().getName() + "!");
				myConnection.writeToClient("s:s:What's your name?");
				playerName = myConnection.readFromClient();
				JeopardyGame.clientName[id - 1] = playerName;
				view.print(playerName + " is in the game. They are thread " + Thread.currentThread().getName() + "\n");
				myConnection.writeToClient("r:p:Awesome! Thank you " + playerName + ". We are waiting for all players to connect.");
				
				JeopardyGame.clientStatus[id - 1] = true;
				
				while(!(JeopardyGame.clientStatus[0] && JeopardyGame.clientStatus[1] && JeopardyGame.clientStatus[2] && counter > 0)) {
					//view.print(playerName + " is waiting for other players. Status: " + JeopardyGame.clientStatus[0] + JeopardyGame.clientStatus[1] + JeopardyGame.clientStatus[2] + "\n");
					Thread.sleep(500);
					counter++;
				}
				
				myConnection.writeToClient("r:p:Everyone's connected! Here's our players ~ \nPlayer 1 is " + clientName[0] + "\nPlayer 2 is " + clientName[1] + "\nPlayer 3 is " + clientName[2] + "\n");
				//Reset client's counter
				counter = 0;
				
				//Wait before setting status back to false
				Thread.sleep(3000);
				JeopardyGame.clientStatus[id - 1] = false;
				
				//Wait until everyone's status are false again
				while(JeopardyGame.clientStatus[0] || JeopardyGame.clientStatus[1] || JeopardyGame.clientStatus[2] || counter == 0)
				{
					//view.print("Waiting for reset: " + id + "\n");
					Thread.sleep(1);
					counter++;
				}
				
				counter = 0;
				
				/*long currentTime = System.currentTimeMillis();
				long projectedTimeMod = currentTime % 10;
				long projectedTime = 0;
				
				
				while(projectedTimeMod < 6 || counter == 0) {
					if(projectedTimeMod < 6)
					{
						currentTime = System.currentTimeMillis();
						projectedTimeMod = currentTime % 10;
						counter++;
					}
					else {
						counter++;
					}
					
				}
				
				projectedTime = currentTime + (projectedTimeMod * 1000);
				
				while(projectedTime > System.currentTimeMillis() || true) {
					double timeleft = (projectedTime - System.currentTimeMillis()) / 1000;
					timeleft = Math.round(timeleft);
					//view.print("Time loop hit for " + id + "projectedTimeMod: " + projectedTimeMod + "\n");
					view.print("Time left until go for : " + id + " is " + timeleft + "\n");
					myConnection.writeToClient("r:p:" + timeleft);
					Thread.sleep(950);
				}*/
				
				
				myConnection.writeToClient("s:s:" + JeopardyGame.QUESTION_ONE + "\n");
				String answer = "";
				
				do {		
					answer = myConnection.readFromClient();
					if(!(answer.equals(JeopardyGame.ANSWER_ONE))) {
						myConnection.writeToClient("s:s:" + "Try again, " + JeopardyGame.QUESTION_ONE);
					}
					else {
						JeopardyGame.winner = playerName;
					}
				}while(JeopardyGame.winner.equals(""));
				
				myConnection.writeToClient("r:p:" + "We have a winner! " + winner + " won!\nThanks for playing!");
				view.print("We have a winner! " + winner + " won!\nThanks for playing!");
				
				//Close connection
				myConnection.closeConnection();
				System.exit(0);
			}	
		} catch (IOException e) {
			view.print("Crap. " + Thread.currentThread().getName() + " had a communciation error.\n");
		} catch (InterruptedException e) {
			view.print("Crap. " + Thread.currentThread().getName() + " had a interrupted thread error.\n");
		}
	}
	
}
