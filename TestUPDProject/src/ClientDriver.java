//This file contains the driver for the UPD Test program between two Amazon Lightsail Instances

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class ClientDriver{
	public static void main(String[] args){
		System.out.println("Hello World! I am complied and have run the client!");

		//Insert more code here
		DatagramSocket dgSocket = null;
		if (args.length < 3) {
			System.out.println("Please pass the arguments in the order: test message, Server host name and Port Number");
			//exit program if no arguments were passed.
			System.exit(1);
		}
		try {
                    dgSocket = new DatagramSocket();
                    byte[] bytes= args[0].getBytes();
                    InetAddress serverHost = InetAddress.getByName(args[1]);
                    int serverPortNumber = Integer.valueOf(args[2]).intValue();
                    DatagramPacket dgRequest = new DatagramPacket(bytes, args[0].length(), serverHost, serverPortNumber);
                    dgSocket.send(dgRequest);
                    byte[] byteBuffer = new byte[1000];
                    DatagramPacket dgResponse = new DatagramPacket(byteBuffer, byteBuffer.length);
                    dgSocket.receive(dgResponse);
                    System.out.println("Datagram response: " + new String(dgResponse.getData()));

		}catch(SocketException e) {
			System.out.println("Socket Exception: " + e.getMessage());
		}catch(IOException e){
			System.out.println("IO Exception: " + e.getMessage());
		}finally{
			if(dgSocket != null){
				dgSocket.close();
			}		
		}
    }
}
