//This file contains the driver for the UPD Test program between two Amazon Lightsail Instances

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class SimpleUPDServer{
	public static void main(String[] args){
		System.out.println("Hello World! I am complied and have run!");
		
		DatagramSocket dgSocket = null;
		if (args.length <= 0) {
			System.out.println("Please pass the port number for the UDPServer");
			//exit program if no arguments were passed.
			System.exit(1);
		}
		try {
                int socketNumber = Integer.valueOf(args[0]).intValue();
                dgSocket = new DatagramSocket(socketNumber);
                
                byte[] byteBuffer = new byte[1000];
                
                
                DatagramPacket dgRequest = new DatagramPacket(byteBuffer, byteBuffer.length);
                dgSocket.receive(dgRequest);

                ScriptEngine engine = new ScriptEngineManager().getEngineByExtension("js");
                String toSend = "";
                String expression = "";
                String fixedExpression = "";

                try{
                    expression = new String(dgRequest.getData());
                    System.out.println("Expression was: " + expression);

                    int counter = 0;
                    while(expression.charAt(counter) != 0){
                        //System.out.println("IN LOOP");
                        //System.out.println(expression.charAt(counter));
                        //char temp = expression.charAt(counter);
                        //int tempInt = temp;
                        //System.out.println("CHARACTER ID" + tempInt);
                        fixedExpression += expression.charAt(counter);
                        counter++;
                    }

                    Object result = engine.eval(fixedExpression);

                    System.out.println("\nAnswer:" + result);
                    toSend = result.toString();


                }catch(ScriptException e){
                    toSend = "ERROR: COULD NOT EVALUATE";
                    e.printStackTrace();
                }


                byte[] bytes = toSend.getBytes();
                DatagramPacket dgresponse = new DatagramPacket(bytes, toSend.length(), dgRequest.getAddress(), dgRequest.getPort());
                dgSocket.send(dgresponse);
                		

		}catch(SocketException e) {
			System.out.println("Socket Exception: " + e.getMessage());
		}catch(IOException e){
			System.out.println("IO Exception: " + e.getMessage());
		}finally{
			if(dgSocket != null){
				dgSocket.close();
			}		
                }
        }
}
