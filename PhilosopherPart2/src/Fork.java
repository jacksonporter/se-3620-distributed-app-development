import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Fork {
	static Logger log = LogManager.getLogger(PhilosopherDriver.class);
	private boolean inUse;
	private int id;
	private int philID;
	
	public Fork(int id)
	{
		this.id = id;
		this.inUse = false;
	}
	
	private synchronized boolean inUse() {
		return this.inUse;
	}
	
	public synchronized boolean setInUse(int philID) {
		log.trace(philID + " is checking to see if fork " + this.id + " is in use.");
		
		if(!this.inUse()) {
			log.trace(philID + " is taking fork " + this.id + ".");
			this.philID = philID;
			this.inUse = true;
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	public int getID() {
		return this.id;
	}
	
	public synchronized void release(int philID) {
		log.trace(philID + " is giving back fork " + this.id + ".");
		if(this.philID == philID) {
			this.inUse = false;
		}
	}
}
