import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Philosopher implements Runnable {
	static Logger log = LogManager.getLogger(PhilosopherDriver.class);
	private int id;
	private Fork fork1, fork2;
	private boolean eating;
	
	public Philosopher(int id, Fork fork1, Fork fork2)
	{
		this.id = id;
		this.fork1 = fork1;
		this.fork2 = fork2;
		this.eating = false;
	}
	
	@Override
	public void run() {
		log.debug("Philosopher " + this.id + " is awake.");
		Random myRand = new Random();
		
		
		while(true) {
			//Think
			try {
				eating = false;
				log.info("I am thinking, and I am philosopher " + this.id + ".");
				Thread.sleep(myRand.nextInt(7000) + 3000);
			} catch (InterruptedException e) {
				log.warn("Philosopher " + this.id + " thread was interuppted in think stage.");
				e.printStackTrace();
			}
			//Eat (when you can)
			
			while(!eating) {
				if(fork1.setInUse(this.id)) {
					log.debug("I, Philosopher " + this.id + " have fork " + fork1.getID());
					
					while(!eating) {
						if(fork2.setInUse(this.id)) {
							eating = true;
							log.debug("I, Philosopher " + this.id + " have fork " + fork2.getID());
							log.info("I, Philosopher " + this.id + ", am eating with Fork " + fork1.getID() + " and Fork " + fork2.getID());
							try {
								Thread.sleep(myRand.nextInt(7000) + 3000);
							} catch (InterruptedException e) {
								System.err.println("Philosopher " + this.id + " thread was interuppted in the eating stage.");
								e.printStackTrace();
							}
							fork1.release(this.id);
							fork2.release(this.id);
							log.info("I, Philosopher " + this.id + ", released Fork " + fork1.getID() + " and Fork " + fork2.getID());
						}
					}
				}
			}
		}
	}
}

